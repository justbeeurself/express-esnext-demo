import { controller, middleware, get, hasSchema, error } from "~/lib/ExpressRouter";
import * as joi from "joi";

const testMiddleware = (req, res, next) => {
    console.log("This is test middleware. It runs before the actual route.");
    return next();
};

const testSchema = {
    query: {
        userId: joi.number().required().error(errors => "User ID must be a number and is required")
    }
};

@controller
export default class Routes {
    constructor(router) { //Optional constructor to directly access the router object on init
        this.router = router;
    }

    //Since this one isn't decorated, it doesn't get used in the router
    getProperty() {
        console.log("TEST");
    }

    @get("/test")
    @middleware(testMiddleware)
    @hasSchema(testSchema)
    testProperty(req, res) {
        res.setPayload(req.validation);
        return res.sendPayload();
        // return res.noContent();
    }

    @error
    errorHandler(err, req, res, next) {
        res.setError(err);
        return res.sendPayload();
    }
}