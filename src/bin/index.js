const path = require("path");
const fs = require("fs");
const setupBabel = require("@babel/register");
const babelOpts = JSON.parse(
    fs.readFileSync(path.join(process.cwd(), "env/.babelrc"))
);

setupBabel(babelOpts);
require("../app");