import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import config from "config";
import { middleware as jsonPayload } from "./middleware/jsonpayload";
import routes from "./routes";

const app = express();
app.use(jsonPayload);
app.use(cors({ origin: config.express.origin }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(routes);

if(process.env.NODE_ENV !== "test") { //Used for unit testing
	const port = config.express.port;
    app.listen(port, () => console.log(`Listening on port ${port}...`));
}

export default app;