import { Router } from "express";
import withSchema from "~/middleware/validation";

export const controller = controller => {
    if(Object.getPrototypeOf(controller) == Router) {
        return controller; //Already is a router, no need to wrap
    }

    let router = Router();
    let inst = new controller(router);
    inst.router = router;

    let potential = Object.getOwnPropertyNames(controller.prototype).filter(x => x != "constructor");

    for(let name of potential) {
        let route = inst[name];

        if(route && route._route) {
            route(router, inst);
        }
    }

    let errorHandler = potential.find(x => inst[x]._errorHandler);

    if(errorHandler) {
        router.use(inst[errorHandler]); //Do error handling last
    }

    return router;
};

const bindToRoute = (method, endpoint, middleware, descriptor) => {
    const oldHandler = descriptor.value;

    if(descriptor.value._middleware && descriptor.value._middleware.length) {
        middleware = middleware.concat(descriptor.value._middleware);
    }

    if(endpoint === -1) {
        descriptor.value = (router, inst) => router[method](oldHandler.bind(inst));
    } else {
        descriptor.value = (router, inst) => router[method](endpoint, ...middleware, oldHandler.bind(inst));
    }

    descriptor.value._route = true;
};

export const get = (endpoint, ...middleware) => (target, _, descriptor) => bindToRoute("get", endpoint, middleware, descriptor);
export const put = (endpoint, ...middleware) => (target, _, descriptor) => bindToRoute("put", endpoint, middleware, descriptor);
export const post = (endpoint, ...middleware) => (target, _, descriptor) => bindToRoute("post", endpoint, middleware, descriptor);
export const remove = (endpoint, ...middleware) => (target, _, descriptor) => bindToRoute("delete", endpoint, middleware, descriptor);
export const use = (endpoint, ...middleware) => (target, _, descriptor) => bindToRoute("post", endpoint, middleware, descriptor);
export const error = (target, _, descriptor) => {
    descriptor.value._errorHandler = true;
};

export const middleware = (...middleware) => (target, _, descriptor) => {
    if(descriptor) { //Method
        if(descriptor.value && typeof descriptor.value === "function") {
            if(!descriptor.value._middleware) {
                descriptor.value._middleware = middleware;
            } else {
                descriptor.value._middleware = descriptor.value._middleware.concat(middleware);
            }
        } else { //Property
            //TODO: How do we register it?
        }
    } else { //Class
        if(Object.getPrototypeOf(target) == Router) {
            target.use(...middleware);
            return target;
        }
    }
};

export const debug = (target, name, descriptor) => {
    console.log("Target", target);
    console.log("Descriptor", descriptor);
};

export const hasSchema = (schema, options) => (target, _, descriptor) => {
    if(!descriptor.value._middleware) {
        descriptor.value._middleware = [withSchema(schema, options)];
    } else {
        descriptor.value._middleware.unshift(withSchema(schema, options));
    }
};