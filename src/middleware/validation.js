const joi = require("joi");

exports = module.exports = ({
    headers, path, query, body
}, options) => {
    let headerSchema = headers ? joi.object(headers) : null;
    let pathSchema = path ? joi.object(path) : null;
    let querySchema = query ? joi.object(query) : null;
    let bodySchema = body ? joi.object(body) : null;

    const handleError = (error, res) => {
        if(error.name == "ValidationError") {
            res.payload.setValidationError(error.details.map(x => x.message));
            return res.sendPayload();
        } else {
            res.setError(error);
            return res.sendError();
        }
    };

    return (req, res, next) => {
        try {
            req.validation = {};
    
            if(headerSchema) {
                let { error, value } = headerSchema.validate(req.headers, { ...options, allowUnknown: true });
                if(error) return handleError(error, res);
    
                req.validation.headers = value;
            }
    
            if(pathSchema) {
                let { error, value } = pathSchema.validate(req.params, options);
                if(error) return handleError(error, res);
    
                req.validation.path = value;
            }
    
            if(querySchema) {
                let { error, value } = querySchema.validate(req.query, options);
                if(error) return handleError(error, res);
    
                req.validation.query = value;
            }
    
            if(bodySchema) {
                let { error, value } = bodySchema.validate(req.body, options);
                if(error) return handleError(error, res);
    
                req.validation.body = value;
            }
    
            return next();
        } catch(ex) {
            return next(ex);
        }
    };
};