export class JsonPayload {
    payload = null;
    error = null;
    status = 200;

    constructor(res) { //TODO: There should be a better way than to pass Response to the payload...
        this.res = res;
    }

    setError = (error, status = 500) => {
        this.error = error;
        this.status = status;
        this.payload = null;
    }

    setPayload = (payload, status = 200) => {
        this.payload = payload;
        this.status = status;
    }

    setValidationError = (error, status = 400) => {
        this.error = {
            name: "validation",
            problems: Array.isArray(error) ? error : [error]
        };
        this.status = status;
    }

    noContent = () => {
        if(this.res) {
            this.res.status(204).send(null);
        }
    }

    send = () => {
        if(this.res) {
            console.log(this.error, this.payload);

            this.res.header("Content-Type", "application/json");
            this.res.status(this.status);
            this.res.send({
                error: this.error,
                payload: this.payload
            });
        }
    }
}

export const middleware = (req, res, next) => {
    res.payload = new JsonPayload(res);
    res.setPayload = res.payload.setPayload;
    res.setError = res.payload.setError;
    res.noContent = res.payload.noContent;

    res.sendPayload = (...args) => {
        if(args.length) {
            res.payload.setPayload(...args);
            return res.payload.send();
        } else return res.payload.send();
    };

    res.sendError = (error, status) => {
        res.setError(error, status);
        return res.sendPayload();
    };

    return next();
};

export default middleware;