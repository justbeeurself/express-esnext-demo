import { expect } from "chai";
import request from "supertest";
import app from "~/app";

describe("App Root", () => {
    it("exports", () => {
        expect(app).to.not.equal(undefined);
    });
	
	it("Works?", done => {
		request(app)
			.get("/")
			.expect("Content-Type", /text\/plain/)
			.expect(200, done);
	});
});