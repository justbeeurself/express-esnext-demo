const setupBabel = require("@babel/register");
const path = require("path");
const fs = require("fs");

const babelOpts = JSON.parse(
    fs.readFileSync(path.join(process.cwd(), "env/.babelrc"))
);

setupBabel(babelOpts);