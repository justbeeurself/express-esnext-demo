import { expect } from "chai";
import jsonPayloadMiddleware, { JsonPayload } from "~/middleware/jsonpayload";

describe("JSONPayload Middleware", () => {
    let res = {};

    const runMiddleware = () => new Promise(resolve => jsonPayloadMiddleware({}, res, resolve));

    beforeEach(() => {
        res = {}
    });

    it("Response Injection", async() => {
        await runMiddleware();

        expect(res).to.haveOwnProperty("payload");
        expect(res.payload).to.haveOwnProperty("error");
        expect(res.payload).to.haveOwnProperty("payload");
        expect(res.payload).to.haveOwnProperty("status");
    });

    it(".setError()", async() => {
        await runMiddleware();
        res.payload.setError("Test error");

        expect(res.payload.error).to.equal("Test error");
        expect(res.payload.payload).to.equal(null);
        expect(res.payload.status).to.equal(500);
    });
});