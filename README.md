# Express-ESNext-Demo

This repository outlines a potential standard for Express RESTful APIs using the latest ECMAScript syntax. It includes a decorator-based routing setup, a clean JSONPayload response format, built-in joi validation for headers, route params, POST bodies and query params.

**Table of Contents**

[TOC]

## Getting Started

* Clone repo
* `npm install`
* `npm start`

## Project Layout Features
* Out-of-the-box testing suite
* Clean, strict ESLint config
* Nodemon config for testing against live changes
* Istanbul code coverage using `nyc` 

## Language Features

### [Decorators](https://babeljs.io/docs/en/babel-plugin-proposal-decorators)
In this demo all routing uses the proposed decorators found in `src/lib/ExpressRouter`
```javascript
@controller
export default class UserRoutes {
    @get("/")
    getAllUsers(req, res, next) {}
}
```

### [Root-Level Imports](https://github.com/tleunen/babel-plugin-module-resolver)
Prevents things like `require("../../../../");`
```javascript
import { router, get } from "~/lib/ExpressRouter";
```

### [Class Properties](https://babeljs.io/docs/en/babel-plugin-proposal-class-properties)
Clean replacement to instantiating properties in the constructor of a class.

I recommend placing properties at the top of the class, above even the constructor.
```javascript
class JsonPayload {
    payload = null;
    error = null;
    status = 200;

    send = () => ...
}
```

### [Optional Catch Binding](https://babeljs.io/docs/en/babel-plugin-proposal-optional-catch-binding)
It isn't always necessary to have access to the error object when catching an exception. Pairs well with the stricter ESLint validation enabled in this repo.
```javascript
try {
    throw new Error("If you don't need the error object, it can be omitted from the catch.");
} catch {
    //TODO: Error handling?
}
```

### [Optional Chaining](https://babeljs.io/docs/en/babel-plugin-proposal-optional-chaining)
This is helpful when dealing with potential nulls. Behaves just like the C# equivalent.
```javascript
@get("/isAdmin")
async isUserAdmin(req, res, next) {
    let { query } = req.validation;
    let user = await UserService.getById(query.userId);
    
    res.setPayload(user?.IsAdmin || false); //The user may not exist
    return res.sendPayload();
}
```

### [Nullish Coalescing Operator](https://babeljs.io/docs/en/babel-plugin-proposal-nullish-coalescing-operator)
Another C#-similar feature
```javascript
return nullValue ?? defaultValue;
```

### [Function Binding](https://babeljs.io/docs/en/babel-plugin-transform-function-bind)
A syntactic alternative to calling `.bind()` or `.call()` on functions. See the documentation for more information, there are several different ways to use this feature, below is only one use case.
```javascript
class Test {
    run(cb) {
        return cb("Hello world!");
    }
}

class Test2 {
    cb(res) {
        console.log("Response:", res);
    }
}

const runner = new Test();
const responder = new Test2();

runner.run(::responder.cb);
```

## Recommendations
I strongly recommend creating Docker-based microservices with this. Because of the way routes are defined with this proposal, it is not recommended to have monolithic applications, otherwise with the combination of schema definitions and attributes everywhere, it will lead to a mess that is hard to read, and even harder to maintain.